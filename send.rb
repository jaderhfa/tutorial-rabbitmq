#!/usr/bin/env ruby
require 'bunny'

# Conectando com o Broker (RabbitMQ)
connection = Bunny.new(automatically_recover: false) #Para conectar a uma maquina remota: ( hostname:  'rabbit.local' ) 
connection.start

channel = connection.create_channel #Criando canal
queue = channel.queue('teste2') #Definir a fila

channel.default_exchange.publish('Jader Henrique', routing_key: queue.name)
puts " [x] Sent 'MeuNome'"

connection.close #fechar conexão