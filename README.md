# Tutorial Rabbit MQ

## Requisitos

[![RabbitMQ](https://img.shields.io/badge/RabbitMQ-donwload-orange.svg)](https://www.rabbitmq.com/download.html)

[![Ruby](https://img.shields.io/badge/Ruby-donwload-red.svg)](https://www.ruby-lang.org/pt/downloads/)

## Filas e Mensageria

Filas são aplicadas para gerar menor acoplamento entre duas aplicações que trocam muitas requisições e que caso ocorra em um curto espaço de tempo o sistema irá processar algumas requisições mesmo que a quantidades seja muito grande.

A mensageria é uma técnica que visa solucionar a comunicação entre sistemas completamente diferentes de uma maneira confiável. Podemos ter várias plataformas que precisam se comunicar e a mensageria faz com que possamos integrar tais sistemas para que eles possam trocar dados de forma desacoplada.

## Protocolo AMQP

* O que é?

O AMQP é um protocolo de comunicação em rede, tal qual o HTTP, que permite que aplicações se comuniquem.

O AMQP tem como vantagens: 
1.   Produzir um padrão aberto para protocolos de mensageria
2.   Permitir a interoperabilidade entre muitas tecnologias e plataformas.

Como existem diversas aplicações funcionando em muitos Sistemas Operacionais, que são desenvolvidos em diversas linguagens de programação, rodando em várias arquiteturas de hardware e máquinas virtuais, o protocolo não só torna a integração dentre esses vários sistemas diferentes possível, como também permite que produtos diferentes que implementem este protocolo possam trocar informações e isso faz do AMQP o pioneiro bem como a evolução da mensageria.

## O que é RabbitMQ

RabbitMQ é um servidor de mensageria de código aberto (open source) desenvolvido em Erlang, implementado para suportar mensagens em um protocolo denominado Advanced Message Queuing Protocol (AMQP). Ele possibilita lidar com o tráfego de mensagens de forma rápida e confiável, além de ser compatível com diversas linguagens de programação, possuir interface de administração nativa e ser multiplataforma

## Instalação
Antes de instalar o rabbitmq , atualize o sistema usando o seguinte comando.
* apt-get update
* apt-get upgrade

[![1.1](/imagens/installRabbitMq1.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/installRabbitMq1.png)
[![1.2](/imagens/installRabbitMq2.png))](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/installRabbitMq2.png)

#
Instale o pacote de dependências para o rabbitmq .
* apt-get install erlang

[![2.1](/imagens/installRabbitMqErlang.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/installRabbitMqErlang.png)

#
E agora instale o rabbitmq-server
* apt-get install rabbitmq-server

[![3.1](/imagens/RabbitMQInstallServer.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/RabbitMQInstallServer.png)

#
Ativar e iniciar o serviço.
* systemctl enable rabbitmq-server
* systemctl start rabbitmq-server
* systemctl status rabbitmq-server

[![4.1](/imagens/installRabbitMq4.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/installRabbitMq4.png)

#
Agora habilite o console de gerenciamento web rabbitmq
* rabbitmq-plugins enable rabbitmq_management

[![5.1](/imagens/installRabbitMq5.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/installRabbitMq5.png)

#
Crie um usuário Admin e defina a senha.
* rabbitmqctl add_user admin admin
* rabbitmqctl set_user_tags admin administrador
* rabbitmqctl set_permissions -p / admin " . * " " . * " " . * " 

[![6.1](/imagens/installRabbitMq6.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/installRabbitMq6.png)

#
Abrir navegador no endereço e porta 
* localhost:15672 
 
[![7.1](/imagens/localhost15672.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/localhost15672.png)

[![7.2](/imagens/localhost215672.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/localhost215672.png)

[Instalando no Windows](https://dev.senior.com.br/documentacao/instalacao-simplificada/2-instalando-o-rabbitmq/)

#
## Conceito e Execução

* Enviar e Receber mensagens de uma fila nomeada em [Ruby](https://www.rabbitmq.com/tutorials/tutorial-one-ruby.html)

[![Conceito do que iremos aplicar](/imagens/ConceitoRabbitMQ.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/ConceitoRabbitMQ.png)

* Criando o publisher

[![publisher](/imagens/Conceito2.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/Conceito2.png)

```ruby
#!/usr/bin/env ruby
require 'bunny'

# Conectando com o Broker (RabbitMQ)
connection = Bunny.new(automatically_recover: false) #Para conectar a uma maquina remota: ( hostname:  'rabbit.local' ) 
connection.start

channel = connection.create_channel #Criando canal
queue = channel.queue('teste2') #Definir a fila

#Definimos a mensagem
channel.default_exchange.publish('Jader Henrique', routing_key: queue.name)
puts " [x] Sent 'MeuNome'"

connection.close #fechar conexão
```
* Criando o consumer

[![Criando o consumer](/imagens/ConceitoReceber.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/ConceitoReceber.png)

```ruby
require 'bunny'

#Mesma configurações do publisher
connection = Bunny.new(automatically_recover: false)
connection.start

#È criada a fila novamente para garantir que a fila exista
channel = connection.create_channel
#Definimos a fila que vamos receber
queue = channel.queue('teste2')

begin
  puts '[*] Esperando por mensagens. Para sair pressione CTRL + C '
  queue.subscribe(block: true) do |_delivery_info, _properties, body|
    puts " [x] Recebido #{body}"
  end
rescue Interrupt => _
  connection.close

  exit(0)
end
```
## Executando 
* Execute o consumer 
```ruby receive.rb```



* Execute o publisher 
```ruby send.rb```


[![8.1](/imagens/executandoEnviandoeRecebendomsg.png)](https://gitlab.com/jaderhfa/tutorial-rabbitmq/blob/master/imagens/executandoEnviandoeRecebendomsg.png)


