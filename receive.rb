require 'bunny'

#Mesma configurações do publisher
connection = Bunny.new(automatically_recover: false)
connection.start

#È criada a fila novamente para garantir que a fila exista
channel = connection.create_channel
#Definimos a fila que vamos receber
queue = channel.queue('teste2')

begin
  puts '[*] Esperando por mensagens. Para sair pressione CTRL + C '
  queue.subscribe(block: true) do |_delivery_info, _properties, body|
    puts " [x] Recebido #{body}"
  end
rescue Interrupt => _
  connection.close

  exit(0)
end